// Input: nhap chieu rong, chieu dai
// B1: Tao bien chieuRong,chieuDai, chuVi,dienTich
// B2: chuVi = (chieuRong+chieuDai)*2
// B3:dienTich=(chieuDai*chieuRong)
// Output: tinh chu vi, dien tich
// var chieuDai, chieuRong, chuVi, dienTich;
// chieuDai = 7;
// chieuRong = 8;
// chuVi = (chieuDai + chieuRong) * 2;
// dienTich = chieuDai * chieuRong;
// console.log({ chuVi });
// console.log({ dienTich });
function chuVi() {
  var chieuDai = document.getElementById("chieu_dai").value * 1;
  var chieuRong = document.getElementById("chieu_rong").value * 1;
  var chuVi = (chieuDai + chieuRong) * 2;
  document.getElementById(
    "chu_vi"
  ).innerHTML = ` Chu vi hình chữ nhật là ${chuVi}`;
}
function dienTich() {
  var chieuDai = document.getElementById("chieu_dai").value * 1;
  var chieuRong = document.getElementById("chieu_rong").value * 1;
  var dienTich = chieuDai * chieuRong;
  console.log(dienTich);
  document.getElementById(
    "dien_tich"
  ).innerHTML = ` Diện tích hình chữ nhật là ${dienTich}`;
}
